package be.codekata;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {
    private final WebDriver driver;

    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public void acceptCookies() {
        WebElement acceptCookiesButton = driver.findElement(By.id("L2AGLb"));
        acceptCookiesButton.click();
    }

    public void fillSearchText(String searchText) {
        WebElement inputField = driver.findElement(By.name("q"));
        inputField.sendKeys(searchText);
    }

    public void clickSearch() {
        WebElement searchButton = driver.findElement(By.name("q"));
        searchButton.submit();
    }
}

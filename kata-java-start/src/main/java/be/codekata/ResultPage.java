package be.codekata;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ResultPage {
    private final WebDriver driver;

    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }


    public String getFirstResult() {
        WebElement firstResult = driver.findElement(By.tagName("h3"));
        return firstResult.getText();
    }
}

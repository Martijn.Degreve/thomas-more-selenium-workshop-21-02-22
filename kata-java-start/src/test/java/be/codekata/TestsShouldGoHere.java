package be.codekata;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class TestsShouldGoHere {

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        setChromeDriverLocation();
        driver = new ChromeDriver();
        openGoogleHomePage(driver);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void searchTest() {
        SearchPage searchPage = new SearchPage(driver);
        searchPage.acceptCookies();

        searchPage.fillSearchText("Eurofins");
        searchPage.clickSearch();

        ResultPage resultPage = new ResultPage(driver);
        String firstResult = resultPage.getFirstResult();

        assertThat(firstResult,equalTo("Worldwide laboratory testing services - Eurofins Scientific"));
    }

    private void openGoogleHomePage(WebDriver driver) {
        driver.get("https://www.google.be");
    }

    private void setChromeDriverLocation() {
        System.setProperty("webdriver.chrome.driver","C:\\Tooling\\WebDrivers\\chromedriver.exe");
    }
}
